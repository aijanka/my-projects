$(function(){
    $('.dialog').hide();
    $('#followDialog').hide();
    var url = 'http://146.185.154.90:8000/blog/aikojune@gmail.com';
    var lastMessageDate = "";
    var message;
    var firstName;
    var lastName;

    function changeName(){

        $('#saveBtn').on('click', function(){
            firstName = $('#firstName').val();
            lastName = $("#lastName").val();
            $('.dialog').hide();//or create it in html

            $.ajax({
                url: url + '/profile',
                method: 'post',
                data: {
                    firstName: firstName,
                    lastName: lastName
                },
                success: function(data){
                    $('#username').text(data.firstName + " " + data.lastName);

                    console.log(data);
                    console.log(data);
                }
            });
        });

    }

    function viewMessages(data){
        if(Array.isArray(data)){
            // data.forEach(function(dataItem){
            //     var name = $('<b>').text(dataItem.user.firstName + " " +
            //         dataItem.user.lastName + "\n\n");
            //     var text = dataItem.message;
            //     var message = $("<div class='newsItem'>").append(name, text);
            //
            //     $('#news').prepend(message);
            //
            // });

            for(var i = data.length - 20; i < data.length; i++){
                var name = $('<b>').text(data[i].user.firstName + " " +
                    data[i].user.lastName + "\n\n");
                    var text = data[i].message;
                    var message = $("<div class='newsItem'>").append(name, text);

                    $('#news').prepend(message);
            }
            lastMessageDate = data[data.length - 1].datetime;
        } else if(typeof data.message !== 'undefined') {
            var name = $('<b>').text(firstName + " " +
                lastName + "\n\n");
            var text = data.message;
            var message = $("<div class='newsItem'>").append(name, text);

            $('#news').prepend(message);
        }


    }

    function addChangedName(){
        $.ajax({
            url: url + '/profile',
            method: 'get',
            success: function(data){
                $("#username").text(data.firstName + " " + data.lastName);
                firstName = data.firstName;
                lastName = data.lastName;
            }
        }).then(changeName);
    }
    addChangedName();



    $.ajax({
        url: url + '/posts',
        method: 'get'
    })
        .then(function(data){
        viewMessages(data);
        setInterval(function(){
            $.ajax({
                url: url + '/profile?datetime=' + lastMessageDate,
                method: 'get',
                success: viewMessages
            });
        }, 2000);
    });

    $('#send').on('click', function(){
        message = $('#message').val();
        $.ajax({
            url: url + '/posts',
            method: 'post',
            data: {
                message: message
            },
            success: function(data){
                viewMessages(data);

            }
        })
    });

    $('#editProfile').on('click', function(){
        $('.dialog').show();
        addChangedName();
    });

    $('#followBtn').on('click', function(){
        $('#followDialog').show();

        $('#completeFollowBtn').on('click', function(){
            var email = $('#email').val();
            $.ajax({
                url: url + '/subscribe',
                data: { email: email},
                method: 'post',
            }).then(function(){
                location.reload();

                $.ajax({
                    url: url + '/subscribe',
                    method: 'get',
                    success: function(data){
                        viewMessages(data);
                    }
                });
            });
        });
    });

    $('#delete').on('click', function(){
        $.ajax({
            url: url + "/subscribe/delete",
            method: 'post',
        }).then(function(){
            location.reload();

            $.ajax({
                url: url + '/subscribe',
                method: 'get',
            });
        })
    });

    $('#viewSubscribers').on('click', function(){
        $.ajax({
            url: url + '/subscribe',
            method: 'get',
            success: function(data){
                var subsList = $("<div class='subsList'>")
                data.forEach(function(subscriber){
                    subsList.append($('<li>').text(subscriber.email));
                });
                $('#subsListDiv').append(subsList);
            }
        })
    })



});